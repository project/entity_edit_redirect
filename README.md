CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Module allows you to redirect to external edit url when accessing Drupal's
regular entity edit route (entity edit form page). This may be useful if you
distribute the content from one main instance (contentpool) to your secondary
instances (satellites). If you want to redirect editing of the content to the
main instance only then this module is for you.

It contains configurable list of entity types (or even their bundles) and their
corresponding paths. You may configure one general path for all of your nodes
or you can granularize it per bundle (article and page bundle having their own
paths).

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/entity_edit_redirect

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/entity_edit_redirect


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Entity Edit Redirect module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Content Authoring > Entity
       edit redirect to configure settings.
    3. Enter a base URL to redirect to.
    4. Enter the Entity edit path patterns. The redirect will be applied only to
       the entity types (and their bundles) listed here. Enter one record per
       line. The format is {entity_type}.{entity_bundle}:{path_pattern}. For
       example: node.article:/content/{uuid}/edit or user:/users/{uuid}/edit.
       A string "{uuid}" in the path will be replaced by actual uuid of entity.
    5. Save configuration.


MAINTAINERS
-----------

Supporting organizations:

 * drunomics - https://www.drupal.org/drunomics
